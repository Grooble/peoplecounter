# PeopleCounter

A small application I coded to know how many people are in a place due to covid restrictions.

## Install

### Linux & Windows

Go to the [releases page](https://gitlab.com/Grooble/PeopleCounter/-/releases), choose the latest version, go to `Assets` and click on `PeopleCounter-Windows` or `PeopleCounter-Linux` under `Packages`.

### MacOS (os X)

At the moment there is no pre-built installer for MacOS, you have to [build it on your machine](#Build).

## Build

In order to build an installer make sure git and NodeJS are installed on your machine.

First open a terminal and clone the project by entering
```git clone https://gitlab.com/Grooble/peoplecounter.git```

Once this is done go in the project folder with
```cd PeopleCounter```

Run
```npm i```

And finally enter
```npm run dist-mac```
if you are on macOS

```npm run dist-win```
if you are on Windows

```npm run dist-linux```
if you are on linux

The installers are outputed in the `dist` folder